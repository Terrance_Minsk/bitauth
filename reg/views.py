from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.db import models
from django import forms
from django.contrib.auth.models import User
from reg.models import  UserProfile
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect

class UserForm( forms.ModelForm ):
  username = forms.CharField( max_length = 30 )
  firstname = forms.CharField( max_length = 30, required = False )
  lastname = forms.CharField( max_length = 30, required = False )
  email = forms.CharField( max_length = 30 )
  pass1 = forms.CharField( widget = forms.PasswordInput, label = "Password", min_length = 6, max_length = 30 )
  pass2 = forms.CharField( widget = forms.PasswordInput, label = "Passwor once again" )
  
  def clean_pass2( self ):
    if ( self.cleaned_data["pass2"] != self.cleaned_data.get( "pass1", "") ):
      raise forms.ValidationError( "incorrect password" )
    return self.cleaned_data["pass2"]
 
class UserProfileForm( forms.ModelForm ):
  weight = forms.IntegerField( min_value = 10, max_value = 200 )
 
def signup( request ):
  
  user = User()
  userProfile = UserProfile()
  if request.method == "POST":
    userForm = UserForm( request.POST, instance = user )
    userProfileForm = UserProfileForm( request.POST, instance = userProfile )
    if userForm.is_valid() and userProfileForm.is_valid():
      userData = userForm.cleaned_data
      user.username = userData['username']
      user.first_name = userData['firstname']
      user.last_name = userData['lastname']
      user.email = userData['email']
      user.set_password( userData['pass1'] )
      user.save()
 
      userProfile = user.get_profile()
      userProfileData = userProfileForm.cleaned_data
      userProfile.weight = userProfileData['weight']
      userProfile.save()
      user = authenticate( username = userData['username'], password = userData['pass1'] )
      login(request, user)
      return HttpResponseRedirect( "/accounts/profile/" )
  else:
     form = UserCreationForm()
     return render( "accounts/signup.html", { "user_": user, "userProfile": UserProfile, "userForm": UserForm, "userProfileForm": UserProfileForm })

@login_required
def my_profile( request ):
  
  user = request.user
  return render_to_response( "accounts/card.html", { "user": user }, context_instance = RequestContext( request ) )
