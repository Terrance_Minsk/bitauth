"""Auth URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/dev/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
import reg.views
from reg.views import  signup, login, logout, my_profile

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url( r'^accounts/signup/$', signup ),
    url( r'^accounts/login/$', 'django.contrib.auth.views.login', { "template_name": "accounts/login.html" } ),
    url( r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login' ),
]
